package org.review.git;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MainApp {

	public static void main(String[] args) {

		boolean running = true;
		Integer firstNumber = null;
		Integer secondNumber = null;
		Integer thirdNumber = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Hola, este es el aplicativo para realizar sumas!!");
		while (running) {
			firstNumber = getValue(reader, "Ingresa el primer valor para la suma:");
			secondNumber = getValue(reader, "Ingresa el segundo valor para la suma:");
			Integer addingValue = firstNumber + secondNumber;
			System.out.println("El valor de la suma entre "+firstNumber.toString()+" y "+secondNumber.toString()+" es "+addingValue.toString());
			running = anotherAdding(reader);
		}
		System.out.println("Esperamos que vuelvas pronto!");
		System.exit(0);
	}

	private static Integer getValue(BufferedReader reader, String text) {
		Integer intValue = null;
		boolean invalidValue = true;
		while (invalidValue) {
			try {
				System.out.println(text);
				String value = reader.readLine();
				if (value == null || value.trim().isEmpty()) {
					throw new Exception("Debes ingresar un valor.");
				}
				intValue = Integer.parseInt(value);
				invalidValue = false;
			} catch (NumberFormatException e) {
				System.out.println("Ups... El valor que ingresaste es inv�lido!");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return intValue;
	}

	private static boolean anotherAdding(BufferedReader reader) {
		boolean otherAdding = false;
		boolean invalidValue = true;
		while (invalidValue) {
			try {
				System.out.println("Desea hacer otra operaci�n (S->Si   N->No):");
				String option = reader.readLine();
				if((option == null || option.trim().isEmpty()) || 
						!(option.equalsIgnoreCase("S") || option.equalsIgnoreCase("N"))) {
					throw new Exception("Ingresaste una opci�n inv�lida.");
				}
				otherAdding = option.equalsIgnoreCase("S") ? true : false; 
				invalidValue = false;
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return otherAdding;
		
	}

}
